This module allows group admins to change settings normally found in 
/admin/settings/site-information to be space specific.
Also supports google analytics tracking per space if googleanalytics module is 
installed.
